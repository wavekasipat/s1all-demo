import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";
import BasketItem from "./MyBasket/BasketItem";
import OrderItem from "./OrderSummary/OrderItem";
import LoadingOverlay from "./Shared/LoadingOverlay";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class OrderSummary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0,
            ready: true
        };
    }

    componentDidMount() {}

    _format(value, pattern) {
        var i = 0,
            v = value.toString();
        return pattern.replace(/#/g, _ => v[i++]);
    }

    render() {
        const card = this.props.persona.cards[this.props.selectedCardIndex];
        const subTotal = this.props.basket.reduce(
            (a, { fullPrice, amount }) => a + fullPrice * amount,
            0
        );
        const discount = this.props.basket.reduce(
            (a, { fullPrice, price, amount }) =>
                a + (fullPrice - price) * amount,
            0
        );
        const grandTotal = this.props.basket.reduce(
            (a, { price, amount }) => a + price * amount,
            0
        );

        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <TopBar title="Order Summary" />
                <ScrollView>
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center",
                            zIndex: 1
                        }}
                    >
                        <View
                            style={{
                                height: 20
                            }}
                        />
                        {this.props.basket.map((item, index) => {
                            return <OrderItem key={index} item={item} />;
                        })}
                        <View
                            style={{
                                height: 20
                            }}
                        />
                        <View
                            style={{
                                flexDirection: "row"
                            }}
                        >
                            <View style={{ flex: 1 }} />
                            <Text
                                style={{
                                    flex: 1,
                                    fontFamily: "Prompt",
                                    fontSize: 20,
                                    color: "#d3d3d3",
                                    textAlign: "right"
                                    // marginLeft: 50
                                }}
                            >
                                Subtotal:
                            </Text>
                            <Text
                                style={{
                                    flex: 1,
                                    fontFamily: "Prompt",
                                    fontSize: 20,
                                    color: "#d3d3d3",
                                    textAlign: "right",
                                    marginRight: 20
                                }}
                            >
                                {subTotal}
                            </Text>
                        </View>
                        {discount != 0 && (
                            <View
                                style={{
                                    flexDirection: "row"
                                }}
                            >
                                <View style={{ flex: 1 }} />
                                <Text
                                    style={{
                                        flex: 1,
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        color: "#d3d3d3",
                                        textAlign: "right"
                                        // marginLeft: 50
                                    }}
                                >
                                    Discount:
                                </Text>
                                <Text
                                    style={{
                                        flex: 1,
                                        fontFamily: "Prompt",
                                        fontSize: 20,
                                        color: "#d3d3d3",
                                        textAlign: "right",
                                        marginRight: 20
                                    }}
                                >
                                    -{discount}
                                </Text>
                            </View>
                        )}
                        <View
                            style={{
                                flexDirection: "row",
                                marginTop: 15
                            }}
                        >
                            <View style={{ flex: 1 }} />
                            <Text
                                style={{
                                    flex: 1,
                                    fontFamily: "Prompt_Bold",
                                    fontSize: 20,
                                    color: "#f7ce68",
                                    textAlign: "right"
                                    // marginLeft: 50
                                }}
                            >
                                Grand Total:
                            </Text>
                            <Text
                                style={{
                                    flex: 1,
                                    fontFamily: "Prompt_Bold",
                                    fontSize: 20,
                                    color: "#f7ce68",
                                    textAlign: "right",
                                    marginRight: 20
                                }}
                            >
                                {grandTotal}
                            </Text>
                        </View>
                        <LinearGradient
                            style={{
                                width: SCREEN_WIDTH,
                                marginTop: 10
                            }}
                            colors={["#000000", "#434343"]}
                            start={[0, 0.5]}
                            end={[1, 0.5]}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 18,
                                    color: "#fff",
                                    marginLeft: 20,
                                    marginTop: 10
                                }}
                            >
                                Payment Method
                            </Text>
                            <View style={{ height: 10 }} />
                            <View
                                style={{
                                    flexDirection: "row"
                                }}
                            >
                                <View
                                    style={{
                                        flex: 1
                                    }}
                                >
                                    <Image
                                        style={{
                                            width: SCREEN_WIDTH / 2 - 40,
                                            height:
                                                ((SCREEN_WIDTH / 2 - 40) *
                                                    322) /
                                                534,
                                            alignSelf: "center"
                                        }}
                                        source={card.png}
                                    />
                                </View>
                                <View
                                    style={{
                                        flex: 1
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 14,
                                            color: "#fff",
                                            marginLeft: 20
                                        }}
                                    >
                                        {card.cardName}
                                    </Text>
                                    <Text />
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 14,
                                            color: "#fff",
                                            marginLeft: 20
                                        }}
                                    >
                                        {card.name}
                                    </Text>
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 14,
                                            color: "#fff",
                                            marginLeft: 20
                                        }}
                                    >
                                        {this._format(
                                            card.number,
                                            "### ### ### ###"
                                        )}
                                    </Text>
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 14,
                                            color: "#fff",
                                            marginLeft: 20
                                        }}
                                    >
                                        Exp. 12/22
                                    </Text>
                                </View>
                            </View>
                            <View style={{ height: 20 }} />
                        </LinearGradient>
                        <TouchableOpacity
                            onPress={this._handleConfirm}
                            style={{
                                marginTop: 40
                            }}
                        >
                            <LinearGradient
                                colors={["#c19963", "#f7ce68"]}
                                start={[1, 0.5]}
                                end={[0, 0.5]}
                                style={{
                                    width: 189,
                                    height: 38,
                                    borderRadius: 100
                                }}
                            >
                                <Text
                                    style={{
                                        alignSelf: "center",
                                        fontSize: 18,
                                        fontFamily: "Prompt",
                                        marginTop: 4
                                    }}
                                >
                                    Confirm
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        <View style={{ height: 20 }} />
                    </View>
                </ScrollView>

                {!this.state.ready && <LoadingOverlay />}
            </LinearGradient>
        );
    }

    _handleConfirm = async () => {
        this.setState({ ready: false });

        let hd = {};
        hd.customer_id = this.props.persona.custId;
        hd.doctotal = this.props.basket.reduce(
            (a, { price, amount }) => a + price * amount,
            0
        );
        hd.shop_location = "Bangkok";

        let dt = [];
        for (let i = 0; i < this.props.basket.length; i++) {
            const item = this.props.basket[i];
            let tmpDt = {};
            tmpDt.item_code = item.code;
            tmpDt.description = item.detail;
            tmpDt.price = item.price;
            tmpDt.quantity = item.amount;
            tmpDt.linetotal = item.price * item.amount;
            dt.push(tmpDt);
        }

        const card = this.props.persona.cards[this.props.selectedCardIndex];

        let ch = [];
        for (let i = 0; i < this.props.basket.length; i++) {
            const item = this.props.basket[i];
            if (item.proId != "") {
                let tmpCh = {};
                tmpCh.card_id = card.cardId;
                tmpCh.campaign_id = item.proId;
                tmpCh.customer_id = this.props.persona.custId;
                tmpCh.quantity = item.amount;
                tmpCh.spending_point =
                    (item.fullPrice - item.price) * item.amount;
                tmpCh.shop_location = "Bangkok";
                tmpCh.item_code = item.code;
                ch.push(tmpCh);
            }
        }

        let data = {};
        data.hd = hd;
        data.dt = dt;
        data.camp_history = ch;

        // console.log(data);

        await fetch(
            `${Expo.Constants.manifest.extra.fxUrl}Order?code=${
                Expo.Constants.manifest.extra.fxKey
            }`,
            { method: "POST", body: JSON.stringify(data) }
        )
            .then(res => {
                console.log(res);
                this.setState({ ready: true });
                this.props.clearBasket();
                this.props.navigation.navigate("ThankYou");
            })
            .catch(error => {
                this.setState({ ready: true });
                console.log(error.message);
                Alert.alert(
                    "Connection Problem",
                    "Please connect internet and try again",
                    [
                        {
                            text: "Okay",
                            onPress: () => {}
                        }
                    ],
                    { cancelable: false }
                );
            });
    };
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket,
        selectedCardIndex: state.reducer.selectedCardIndex
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OrderSummary);
