import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";
import BasketItem from "./MyBasket/BasketItem";

const SCREEN_WIDTH = Dimensions.get("window").width;

export default class ThankYou extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0
        };
    }

    componentDidMount() {}

    render() {
        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <View
                    style={{
                        flex: 1,
                        alignItems: "center",
                        zIndex: 1
                    }}
                >
                    <View style={{ height: 50 }} />
                    <Text
                        style={{
                            fontFamily: "Prompt",
                            fontSize: 30,
                            color: "#fff"
                        }}
                    >
                        Thank you!
                    </Text>
                    <Text
                        style={{
                            fontFamily: "Prompt",
                            fontSize: 18,
                            color: "#fff"
                        }}
                    >
                        Your order has been placed
                    </Text>
                    <View style={{ height: 20 }} />
                    <SvgUri
                        width={SCREEN_WIDTH - 60}
                        height={SCREEN_WIDTH - 60}
                        source={require("../assets/Welcome/svg/welcome_Thankyou.svg")}
                    />
                    <View style={{ height: 15 }} />
                    <Text
                        style={{
                            fontFamily: "Prompt",
                            fontSize: 22,
                            color: "#fff",
                            textAlign: "center"
                        }}
                    >
                        S1 พร้อมมอบสินค้าให้คุณแล้ว{"\n"}เชิญรับสินค้าได้เลย
                        เย้!
                    </Text>
                </View>
                <BottomBar />
            </LinearGradient>
        );
    }
}
