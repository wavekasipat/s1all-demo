import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
// import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient, BarCodeScanner, Permissions, Camera } from "expo";
import TopBar from "./Shared/TopBar";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

let PERSONAS = [];

class ScanPersona extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasCameraPermission: null,
            scanned: false
        };
    }

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === "granted" });

        PERSONAS = await this.props.personas.reduce((total, value) => {
            return total.concat(value.customer_id);
        }, []);
    }

    render() {
        const { hasCameraPermission } = this.state;

        if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <React.Fragment>
                    <TopBar title="Scan Persona" />
                    <View
                        style={{
                            backgroundColor: "rgba(0,0,0,0.64)",
                            alignItems: "center",
                            justifyContent: "center",
                            flex: 1
                        }}
                    >
                        <View
                            style={{
                                width: SCREEN_WIDTH,
                                position: "absolute",
                                top: 40,
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 20,
                                    letterSpacing: 0.2,
                                    textAlign: "center",
                                    color: "#ffffff"
                                }}
                            >
                                ScanQR บุคคลตัวอย่าง{"\n"}
                                ที่ Lifestyle ตรงกับคุณมากที่สุด
                            </Text>
                        </View>
                        <View
                            style={{
                                width: SCREEN_WIDTH - 40,
                                height: SCREEN_WIDTH - 40,
                                borderWidth: 1,
                                borderColor: "#00b900"
                            }}
                        >
                            <Camera
                                onBarCodeScanned={this._handleBarCodeRead}
                                style={{ flex: 1 }}
                                barCodeScannerSettings={{
                                    barCodeTypes: [
                                        BarCodeScanner.Constants.BarCodeType.qr
                                    ]
                                }}
                            />
                        </View>
                    </View>
                </React.Fragment>
            );
        }
    }

    _handleBarCodeRead = async ({ type, data }) => {
        if (!this.state.scanned && PERSONAS.includes(data)) {
            this.setState({
                scanned: true
            });

            let persona = this.props.personas.reduce((total, value) => {
                if (value.customer_id == data) {
                    total = value;
                }
                return total;
            }, []);

            this.props.updatePersona({
                custId: persona.customer_id,
                name: persona.first_name,
                engName: "",
                describe: persona.segment_name,
                subDescribe: persona.description,
                promoPic: { uri: persona.persona_pic },
                churnPic:
                    persona.churn_pic != null
                        ? { uri: persona.churn_pic }
                        : { uri: "" },
                cards: [],
                promos: [],
                specialPromos: [],
                selectedCardIndex: 1
            });

            this.props.navigation.replace("Home", { fromScan: true });
        }
    };
}

const opacity = "rgba(0, 0, 0, .6)";
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column"
    },
    layerTop: {
        flex: 1,
        backgroundColor: opacity
    },
    layerCenter: {
        flex: 1,
        flexDirection: "row"
    },
    layerLeft: {
        flex: 1,
        backgroundColor: opacity
    },
    focused: {
        flex: 8,
        borderWidth: 1,
        borderColor: "#00b900"
    },
    layerRight: {
        flex: 1,
        backgroundColor: opacity
    },
    layerBottom: {
        flex: 1,
        backgroundColor: opacity
    }
});

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        personas: state.reducer.personas
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScanPersona);
