import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

class Promotion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0,
            promo: {
                png: require("../assets/Element/close.png"),
                promoId: "",
                promoName: "",
                isPersonalized: false,
                itemCode: "",
                itemName: "",
                itemDesc: ""
            }
        };
    }

    componentDidMount() {
        this.setState({
            promo: this.props.navigation.getParam("promo", {
                png: require("../assets/Element/close.png"),
                promoId: "",
                promoName: "",
                isPersonalized: false,
                itemCode: "",
                itemName: "",
                itemDesc: ""
            })
        });
    }

    render() {
        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <TopBar
                    title={
                        this.state.promo.isPersonalized
                            ? "S1 For You"
                            : "Promotion"
                    }
                />
                <ScrollView style={{ zIndex: 1 }}>
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center",
                            zIndex: 1
                        }}
                    >
                        <View style={{ height: 10 }} />

                        <Image
                            style={{
                                width: SCREEN_WIDTH - 70,
                                height: SCREEN_WIDTH - 70
                            }}
                            source={this.state.promo.png}
                        />
                        <View style={{ height: 30 }} />
                        <View style={{ flexDirection: "row" }}>
                            <View style={{ width: 35 }} />
                            <View style={{ flex: 1 }}>
                                {this.state.promo.isPersonalized && (
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            fontSize: 14,
                                            letterSpacing: 0.17,
                                            color: "#f7ce68"
                                        }}
                                    >
                                        {this.state.promo.promoName}
                                    </Text>
                                )}
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 16,
                                        letterSpacing: 0.13,
                                        color: "#ffffff"
                                    }}
                                >
                                    {this.state.promo.itemName}
                                </Text>
                                <Text
                                    style={{
                                        fontFamily: "Prompt_ExtraLight",
                                        fontSize: 16,
                                        letterSpacing: 0.13,
                                        color: "#ffffff"
                                    }}
                                >
                                    {this.state.promo.itemDesc}
                                </Text>
                                <View style={{ height: 10 }} />
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 14,
                                        letterSpacing: 0.17,
                                        color: "#f7ce68"
                                    }}
                                >
                                    เงื่อนไข
                                </Text>
                                <Text
                                    style={{
                                        fontFamily: "Prompt_ExtraLight",
                                        fontSize: 16,
                                        letterSpacing: 0.13,
                                        color: "#ffffff"
                                    }}
                                >
                                    - ไม่สามารถเปลี่ยนเป็นสินค้าอื่นได้
                                    นอกจากที่ S1 คัดสรรให้ {"\n"}- S1
                                    คัดสรรสินค้าจากข้อมูลและพฤติกรรมการซื้อสินค้าระหว่างคุณและ
                                    S1 เท่านั้น
                                </Text>
                            </View>
                            <View style={{ width: 35 }} />
                        </View>
                    </View>
                </ScrollView>
                <BottomBar />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Promotion);
