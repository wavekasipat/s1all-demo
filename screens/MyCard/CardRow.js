import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity
} from "react-native";
import SvgUri from "react-native-svg-uri";
import { LinearGradient } from "expo";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

class CardRow extends React.PureComponent {
    _format(value, pattern) {
        var i = 0,
            v = value.toString();
        return pattern.replace(/#/g, _ => v[i++]);
    }

    render() {
        return (
            <React.Fragment>
                <View style={{ height: 15 }} />
                <TouchableOpacity
                    onPress={() => {
                        this.props.updateSelectedCardIndex(this.props.index);
                    }}
                >
                    <View
                        style={{
                            flexDirection: "row",
                            width: SCREEN_WIDTH
                        }}
                    >
                        <View style={{ flex: 1 }}>
                            <View style={{ height: 17 }} />
                            <LinearGradient
                                style={{
                                    flex: 1,
                                    justifyContent: "center"
                                }}
                                colors={[
                                    this.props.index ==
                                    this.props.selectedCardIndex
                                        ? "#f7ce68"
                                        : "#000000",
                                    "#434343"
                                ]}
                                start={[0, 0.5]}
                                end={[1, 0.5]}
                            >
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 18,
                                        color: "#fff",
                                        marginLeft: 10
                                    }}
                                >
                                    {this.props.item.cardName}
                                </Text>
                                <Text />
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 12,
                                        color: "#fff",
                                        marginLeft: 10
                                    }}
                                >
                                    Balance
                                </Text>
                                <Text
                                    style={{
                                        fontFamily: "Prompt",
                                        fontSize: 22,
                                        color: "#f7ce68",
                                        textAlign: "right",
                                        marginRight: 30
                                    }}
                                >
                                    {this.props.item.balance
                                        .toFixed(2)
                                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")}
                                </Text>
                            </LinearGradient>
                            <View style={{ height: 17 }} />
                        </View>
                        <View style={{ flex: 1 }}>
                            <View
                                style={[
                                    {
                                        width: SCREEN_WIDTH / 1.5,
                                        marginLeft: -15,
                                        borderRadius: 17
                                    },
                                    this.props.index ==
                                    this.props.selectedCardIndex
                                        ? {
                                              backgroundColor: "#ffffff"
                                          }
                                        : {
                                              backgroundColor:
                                                  "rgba(255, 255, 255, 0)"
                                          }
                                ]}
                            >
                                <ImageBackground
                                    style={{
                                        width: SCREEN_WIDTH / 1.5,
                                        height:
                                            ((SCREEN_WIDTH / 1.5) * 322) / 534,
                                        justifyContent: "flex-end",
                                        margin: 6
                                    }}
                                    source={this.props.item.png}
                                >
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            color: "#fff",
                                            marginLeft: 26
                                        }}
                                    >
                                        {this.props.item.name}
                                    </Text>
                                    <Text
                                        style={{
                                            fontFamily: "Prompt",
                                            color: "#fff",
                                            marginLeft: 24,
                                            marginBottom: 20,
                                            letterSpacing: 4
                                        }}
                                    >
                                        {this._format(
                                            this.props.item.number,
                                            "### ### ### ###"
                                        )}
                                    </Text>
                                </ImageBackground>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{ height: 15 }} />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        basket: state.reducer.basket,
        selectedCardIndex: state.reducer.selectedCardIndex
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CardRow);
