import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient } from "expo";
import SvgUri from "react-native-svg-uri";
import Carousel, { Pagination } from "react-native-snap-carousel";
import TopBar from "./Shared/TopBar";
import BottomBar from "./Shared/BottomBar";
import BasketItem from "./MyBasket/BasketItem";
import Promotion from "./Promotions/Promotion";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

class Promotions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeSlide: 0
        };
    }

    componentDidMount() {}

    _renderPromotion = ({ item, index }) => {
        return <Promotion item={item} />;
    };

    render() {
        return (
            <LinearGradient
                style={{
                    flex: 1
                }}
                colors={["#000000", "#434343"]}
                start={[1, 0.5]}
                end={[0, 0.5]}
            >
                <TopBar title="Promotions" />
                <ScrollView style={{ zIndex: 1 }}>
                    <View
                        style={{
                            flex: 1,
                            alignItems: "center",
                            zIndex: 1
                        }}
                    >
                        <View style={{ height: 10 }} />
                        <Text
                            style={{
                                fontFamily: "Prompt",
                                fontSize: 20,
                                color: "#d3d3d3"
                            }}
                        >
                            Special for you
                        </Text>
                        <View style={{ height: 10 }} />
                        <View
                            style={{
                                height: (SCREEN_HEIGHT - 170) / 2 - 60
                            }}
                        >
                            <Carousel
                                data={this.props.persona.specialPromos}
                                renderItem={this._renderPromotion}
                                sliderWidth={SCREEN_WIDTH}
                                itemWidth={(SCREEN_HEIGHT - 170) / 2 - 50}
                                enableSnap={false}
                                inactiveSlideOpacity={0.9}
                                inactiveSlideScale={1}
                                // activeSlideAlignment="start"
                            />
                        </View>
                        <View style={{ height: 10 }} />
                        <Text
                            style={{
                                fontFamily: "Prompt",
                                fontSize: 20,
                                color: "#d3d3d3"
                            }}
                        >
                            All Promotions
                        </Text>
                        <View style={{ height: 10 }} />
                        <View
                            style={{
                                height: (SCREEN_HEIGHT - 170) / 2 - 60
                            }}
                        >
                            <Carousel
                                data={this.props.persona.promos}
                                renderItem={this._renderPromotion}
                                sliderWidth={SCREEN_WIDTH}
                                itemWidth={(SCREEN_HEIGHT - 170) / 2 - 50}
                                enableSnap={false}
                                inactiveSlideOpacity={0.9}
                                inactiveSlideScale={1}
                                // activeSlideAlignment="start"
                            />
                        </View>
                    </View>
                </ScrollView>
                <BottomBar />
            </LinearGradient>
        );
    }
}

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Promotions);
