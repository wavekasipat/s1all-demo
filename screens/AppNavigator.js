import { createStackNavigator, createAppContainer } from "react-navigation";
import { fromRight } from "react-navigation-transitions";

import Welcome from "./Welcome";
import ScanPersona from "./ScanPersona";
import Home from "./Home";
import ScanProduct from "./ScanProduct";
import AddToBasket from "./AddToBasket";
import MyBasket from "./MyBasket";
import MyCard from "./MyCard";
import OrderSummary from "./OrderSummary";
import ThankYou from "./ThankYou";
import Promotion from "./Promotion";

const AppNavigator = createStackNavigator(
    {
        Welcome: Welcome,
        ScanPersona: ScanPersona,
        Home: Home,
        ScanProduct: ScanProduct,
        AddToBasket: AddToBasket,
        MyBasket: MyBasket,
        MyCard: MyCard,
        OrderSummary: OrderSummary,
        ThankYou: ThankYou,
        Promotion: Promotion
    },
    {
        initialRouteName: "Welcome",
        headerMode: "none"
        // transitionConfig: () => fromRight()
    }
);

export default createAppContainer(AppNavigator);
