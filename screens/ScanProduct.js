import React from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    Dimensions,
    ImageBackground,
    Picker,
    KeyboardAvoidingView,
    Alert,
    TextInput,
    TouchableOpacity,
    WebView
} from "react-native";
// import { styles } from "../styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";
import { LinearGradient, BarCodeScanner, Permissions, Camera } from "expo";
import TopBar from "./Shared/TopBar";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../action";

const SCREEN_WIDTH = Dimensions.get("window").width;

let ITEMS = [];

class ScanProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasCameraPermission: null
        };
    }

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === "granted" });

        ITEMS = await this.props.items.reduce((total, value) => {
            return total.concat(value.item_code);
        }, []);
    }

    render() {
        const { hasCameraPermission } = this.state;

        if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <React.Fragment>
                    <TopBar title="Scan Product" />
                    <View
                        style={{
                            backgroundColor: "rgba(0,0,0,0.64)",
                            alignItems: "center",
                            justifyContent: "center",
                            flex: 1
                        }}
                    >
                        <View
                            style={{
                                width: SCREEN_WIDTH,
                                position: "absolute",
                                top: 40,
                                alignItems: "center"
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: "Prompt",
                                    fontSize: 20,
                                    letterSpacing: 0.2,
                                    textAlign: "center",
                                    color: "#ffffff"
                                }}
                            >
                                เริ่ม ScanQR ที่สินค้า{"\n"}
                                ที่คุณถูกใจได้เลย!
                            </Text>
                        </View>
                        <View
                            style={{
                                width: SCREEN_WIDTH - 40,
                                height: SCREEN_WIDTH - 40,
                                borderWidth: 1,
                                borderColor: "#00b900"
                            }}
                        >
                            <Camera
                                onBarCodeScanned={this._handleBarCodeRead}
                                style={{ flex: 1 }}
                                barCodeScannerSettings={{
                                    barCodeTypes: [
                                        BarCodeScanner.Constants.BarCodeType.qr
                                    ]
                                }}
                            />
                        </View>
                    </View>
                </React.Fragment>
            );
        }
    }

    _handleBarCodeRead = ({ type, data }) => {
        if (ITEMS.includes(data)) {
            let item = this.props.items.reduce((total, value) => {
                if (value.item_code == data) {
                    total = value;
                }
                return total;
            }, []);

            // let promos = this.props.persona.promos.reduce((total, value) => {
            //     return total.concat(value.itemCode);
            // }, []);

            let allPromos = this.props.persona.allPromos.reduce(
                (total, value) => {
                    return total.concat(value.itemCode);
                },
                []
            );

            let matchPromo = this.props.persona.allPromos.reduce(
                (total, value) => {
                    if (value.itemCode == data) {
                        total = value;
                    }
                    return total;
                },
                []
            );

            let scannedItem = {
                code: item.item_code,
                pic: { uri: item.item_pic },
                name: item.item_name,
                detail: item.item_desc,
                remark: allPromos.includes(data) ? "S1คัดสรรเพื่อคุณ" : "",
                fullPrice: item.sales_price,
                price: allPromos.includes(data)
                    ? matchPromo.promoPrice
                    : item.sales_price,
                amount: 1,
                proId: allPromos.includes(data) ? matchPromo.promoId : ""
            };

            this.props.navigation.replace("AddToBasket", {
                scannedItem: scannedItem
            });
        }
    };
}

const opacity = "rgba(0, 0, 0, .6)";
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column"
    },
    layerTop: {
        flex: 1,
        backgroundColor: opacity
    },
    layerCenter: {
        flex: 1,
        flexDirection: "row"
    },
    layerLeft: {
        flex: 1,
        backgroundColor: opacity
    },
    focused: {
        flex: 8,
        borderWidth: 1,
        borderColor: "#00b900"
    },
    layerRight: {
        flex: 1,
        backgroundColor: opacity
    },
    layerBottom: {
        flex: 1,
        backgroundColor: opacity
    }
});

const mapStateToProps = state => {
    return {
        persona: state.reducer.persona,
        personas: state.reducer.personas,
        items: state.reducer.items
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(Actions, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScanProduct);
