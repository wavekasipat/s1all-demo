import React from "react";
import { StyleSheet, Text, View, BackHandler } from "react-native";
import { Font } from "expo";

import { Provider } from "react-redux";
import configureStore from "./store";
import AppNavigator from "./screens/AppNavigator";

const store = configureStore();

export default class App extends React.Component {
    state = {
        fontLoaded: false
    };

    async componentDidMount() {
        await Font.loadAsync({
            Prompt: require("./assets/fonts/Prompt-Regular.ttf"),
            Prompt_Bold: require("./assets/fonts/Prompt-Bold.ttf"),
            Prompt_LightItalic: require("./assets/fonts/Prompt-LightItalic.ttf"),
            Prompt_ExtraLight: require("./assets/fonts/Prompt-ExtraLight.ttf")
        });

        this.setState({ fontLoaded: true });
        BackHandler.addEventListener("hardwareBackPress", function() {
            return true;
        });
    }

    render() {
        return (
            <Provider store={store}>
                {this.state.fontLoaded ? <AppNavigator /> : null}
            </Provider>
        );
    }
}
